#! /usr/bin/env node
import program from 'commander'
import chalk from "chalk";
import figlet from 'figlet'

console.log(
    chalk.red(
        figlet.textSync('HTML-CLI', { horizontalLayout: 'full' })
    )
);

program
    .version('1.0.0')
    .description('version');

program
    .description('generate new project')
    .command('new <name>','generate new project', { executableFile: 'index-new' })
    .command('add <pageName>','add new page', { executableFile: 'index-add' })




program.parse(process.argv);
