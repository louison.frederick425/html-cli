import path from "path";
import listr from "listr";
import execa from "execa";
import ncp from "ncp";
import chalk from "chalk";
import program from 'commander'


program.parse(process.argv);

const name = program.args[0];

    const currentFileUrl = import.meta.url.split('///')[1];
    const templateDir = path.resolve(
        new URL(currentFileUrl).pathname,
        '../templates/'
    );
    const task = new listr([
        {
            title: 'Create page folder ',
            task:async () => {
                const result = await execa('mkdir', [name], {
                    cwd: './',
                });
                if (result.failed) {
                    return Promise.reject(new Error('Failed to initialize materialize'));
                }
                return;
            },
        },
        {
            title: 'Copy pages files',
            task: () => {
                ncp(templateDir+'/page','./'+name, function (err) {
                    if (err) {
                        return console.error(err);
                    }
                });
            },
        },
    ])
     task.run();
    console.log('%s Project ready', chalk.green.bold('DONE'));

