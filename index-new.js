import inquirer from 'inquirer'
import listr from 'listr'
import path from 'path';
import execa from "execa";
import chalk from "chalk";
import ncp from 'ncp';
async function initGit(folder) {
    const result = await execa('git', ['init'], {
        cwd: './'+folder,
    });
    if (result.failed) {
        return Promise.reject(new Error('Failed to initialize git'));
    }
    return;
}
import program from 'commander'


program.parse(process.argv);

const name = program.args[0];

    inquirer.prompt([
        {
            type:'list',
            name:'style',
            choices:[
                'bootstrap',
                'materialize'
            ]
        },
        {
            type:'comfirm',
            name:'git',
            message:'init git repository ?',
            default:true
        }
    ]).then(async(answer)=>{
        const currentFileUrl = import.meta.url.split('///')[1];
        const templateDir = path.resolve(
            new URL(currentFileUrl).pathname,
            '../templates/'
        );
        const tasks = new listr([
            {
                title: 'Create folder '+name,
                task:async () => {
                    const result = await execa('mkdir', [name], {
                        cwd: './',
                    });
                    if (result.failed) {
                        return Promise.reject(new Error('Failed to initialize git'));
                    }
                    return;
                },
            },
            {
                title: 'init git repository',
                task: () => {
                    initGit(name).then(_ =>{});
                },
            },
            {
                title: 'Copy project files',
                task: () => {
                    ncp(templateDir+'/project','./'+name, function (err) {
                        if (err) {
                            return console.error(err);
                        }
                    });
                },
            },
            {
                title: 'Create style folder ',
                enabled:_=> {
                    return answer.style === 'bootstrap'
                },
                task:async () => {
                    const result = await execa('mkdir', ['bootstrap'], {
                        cwd: './'+name,
                    });
                    if (result.failed) {
                        return Promise.reject(new Error('Failed to initialize bootstrap'));
                    }
                    return;
                },
            },
            {
                title: 'Copy project files',
                enabled:_=>{
                    return answer.style === 'bootstrap'
                },
                task: () => {
                    console.log('bootstrap')
                    ncp(templateDir+'/options/bootstrap','./'+name+'/bootstrap', function (err) {
                        if (err) {
                            return console.error(err);
                        }
                    });
                },
            },
            {
                title: 'Create style folder ',
                enabled:_=>{
                    return answer.style === 'materialize'
                },
                task:async () => {
                    const result = await execa('mkdir', ['materialize'], {
                        cwd: './'+name,
                    });
                    if (result.failed) {
                        return Promise.reject(new Error('Failed to initialize materialize'));
                    }
                    return;
                },
            },
            {
                title: 'Copy project files',
                enabled:_=>{
                    return answer.style === 'materialize'
                },
                task: () => {
                    ncp(templateDir+'/options/materialize','./'+name+'/materialize', function (err) {
                        if (err) {
                            return console.error(err);
                        }
                    });
                },
            },

        ]);
        await tasks.run();
        console.log('%s Project ready', chalk.green.bold('DONE'));
        console.log(chalk.green.bold("now you can do cd <'your project name'>"));
        console.log(chalk.green.bold("and use html-cli add <'your page name'> to create a new page"));
    })


